package com.example.yoel.pkbi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();

    }

    public void Huruf(View view){
        Intent intent = new Intent(Main2Activity.this, Huruf.class);
        Main2Activity.this.startActivity(intent);
    }
    public void Angka(View view){
        Intent intent = new Intent(Main2Activity.this, Angka.class);
        Main2Activity.this.startActivity(intent);
    }
    public void Kata(View view){
        Intent intent = new Intent(Main2Activity.this, Kata.class);
        Main2Activity.this.startActivity(intent);
    }
}