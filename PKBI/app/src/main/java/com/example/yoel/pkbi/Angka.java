package com.example.yoel.pkbi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Angka extends AppCompatActivity {
    ArrayList<String> angka;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angka);
        Intent intent = getIntent();
        // Get the reference of movies
        ListView angkaList=(ListView)findViewById(R.id.listView);

        angka = new ArrayList<String>();
        getangka();

        // Create The Adapter with passing ArrayList as 3rd parameter
        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, angka);
        // Set The Adapter
        angkaList.setAdapter(arrayAdapter);

        // register onClickListener to handle click events on each item
        angkaList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            // argument position gives the index of item which is clicked
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                String selectedmovie=angka.get(position);
                Toast.makeText(getApplicationContext(), "Movie Selected : "+selectedmovie,   Toast.LENGTH_LONG).show();
            }
        });
    }

    void getangka()
    {
        angka.add("0");
        angka.add("1");
        angka.add("2");
        angka.add("3");
        angka.add("4");
        angka.add("5");
        angka.add("6");
        angka.add("7");
        angka.add("8");
        angka.add("9");
    }
}
