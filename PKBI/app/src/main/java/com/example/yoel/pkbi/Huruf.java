package com.example.yoel.pkbi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Huruf extends AppCompatActivity {

    ArrayList<String> huruf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huruf);
        Intent intent = getIntent();

        // Get the reference of movies
        ListView hurufList=(ListView)findViewById(R.id.listView);

        huruf = new ArrayList<String>();
        gethuruf();

        // Create The Adapter with passing ArrayList as 3rd parameter
        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, huruf);
        // Set The Adapter
        hurufList.setAdapter(arrayAdapter);

        // register onClickListener to handle click events on each item
        hurufList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            // argument position gives the index of item which is clicked
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                String selectedmovie=huruf.get(position);
                Toast.makeText(getApplicationContext(), "Movie Selected : "+selectedmovie,   Toast.LENGTH_LONG).show();
            }
        });
    }

    void gethuruf()
    {
        huruf.add("A");
        huruf.add("B");
        huruf.add("C");
        huruf.add("D");
        huruf.add("E");
        huruf.add("F");
        huruf.add("G");
        huruf.add("H");
        huruf.add("I");
        huruf.add("J");
        huruf.add("K");
        huruf.add("L");
        huruf.add("M");
        huruf.add("N");
        huruf.add("O");
        huruf.add("P");
        huruf.add("Q");
        huruf.add("R");
        huruf.add("S");
        huruf.add("T");
        huruf.add("U");
        huruf.add("V");
        huruf.add("W");
        huruf.add("X");
        huruf.add("Y");
        huruf.add("Z");
    }
}